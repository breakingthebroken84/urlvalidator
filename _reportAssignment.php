<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/report.css">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head>
<body>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">

<?php 
	require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
	require_once(dirname(__FILE__).'/lib.php');
	require_once(dirname(__FILE__).'/classes/observer.php');
	require_once(dirname(__FILE__).'/_pastEvaluation.php');

	global $DB, $USER;

	$admins = get_admins();
	$isadmin = false;
	foreach ($admins as $admin) {
		if ($USER->id == $admin->id)
		{
			$isadmin = true;
			break;
		}
	}
	if (!$isadmin) {
		die();
	}

	$courseId = $_GET['course'];
	$courseModuleId = $_GET['module'];
	$assignmentId = $_GET['assignment'];
	$assignmentChecker = new mod_assignmentchecker_observer();
	
?>

<div class="container-fluid">
<?php

	$assignment = $DB->get_record(
	    "assign",
	    array(
	        "id" => $assignmentId
	    )
	);
	$maxGrade = $assignment->grade;
	floatval($maxGrade);
	$maxGrade = floatval($maxGrade);	

	$submissions = $DB->get_records(
		"assignsubmission_onlinetext",
		array(
			"assignment" => $assignmentId
		)
		//'timemodified' //sort
	);

	echo '<br><div class="text-center"><h4><b>'.$assignment->name.'</b> <small>('.count($submissions).' submissions)</small></h4>';
	//echo '<small>Valutatore associato: <b>'.$pluginInstance->name.'</b>  (id: '.$pluginInstance->id.')</small></div><br>';

?>
</div>

<div class="container-fluid">
<div class="row">
<div class="col-sm-12">

	<div class="resume">

		<div class="resume-table">
		<table class="table table-fixed">
			<thead>
				<th>submission</th>
				<th>user</th>
				<th>assignment</th>
				<th>submission</th>
				<th>onlinetext</th>
				<th>grade<!-- / <?php echo $maxGrade ?>--></th>
				<th>completion</th>
				<!--<th>$pluginInstanceName</th>-->

				<th>UrlExists<br><small><?php //echo $pluginInstanceUrlExists ?>&nbsp;</small></th>
				<th>UrlIsUnique<br><small><?php //echo $pluginInstanceUrlIsUnique ?>&nbsp;</small></th>
				<!--<th>UrlIsCorrect<br><small><?php //echo $pluginInstanceUrlIsCorrect ?>&nbsp;</small></th>--><!-- la correttezza è un aggregato dei controlli successivi -->
				<th>UrlContains<br><small><?php //echo $pluginInstanceUrlContains ?>&nbsp;</small></th>
				<th>UrlStartsWith<br><small><?php //echo $pluginInstanceUrlStartsWith ?>&nbsp;</small></th>
				<th>UrlEndsWith<br><small><?php //echo $pluginInstanceUrlEndsWith ?>&nbsp;</small></th>
				<th>UrlUrlMatchesRegex<br><small><?php //echo $pluginInstanceUrlUrlMatchesRegex ?>&nbsp;</small></th>
				<th>MinChar<br><small><?php //echo $pluginInstanceMinChar ?>&nbsp;</small></th>
				<th>MaxChar<br><small><?php //echo $pluginInstanceMaxChar ?>&nbsp;</small></th>
				<th>MinWords<br><small><?php //echo $pluginInstanceMinWords ?>&nbsp;</small></th>
				<th>MaxWords<br><small><?php //echo $pluginInstanceMaxWords ?>&nbsp;</small></th>

				<th class="text-center">PREVISIONE<br><small>valutazione pregresso</small></th>
			</thead>
			<tbody>
				<?php

				$total = 0;
				$positive = 0;
				$nonPositive = 0;
				$nonPositiveToPositive = 0;
				$nonPositiveToNegative = 0;

				foreach ($submissions as $key => $submission) {

					$overall = true;

					$onlinetext = $submission->onlinetext;
					$onlinetext = str_replace('&','&amp;',$onlinetext);
					$onlinetext = str_replace('<','&lt;',$onlinetext);
					$onlinetext = str_replace('>','&gt;',$onlinetext);
					$userId = $DB->get_record(
						"assign_submission",
						array(
							"id" => $submission->submission
						)
					)->userid;

					$user = $DB->get_record(
						"user",
						array(
							"id" => $userId
						)
					);

					$grade = $DB->get_record(
						"assign_grades",
						array(
							"assignment" => $assignment->id,
							"userid" => $userId
						)
					)->grade;

					$grade = floatval($grade);

					// completion state
					$completionState = $DB->get_record(
						'course_modules_completion',
						array(
							"coursemoduleid" 	=> $courseModuleId,
							"userid" 			=> $userId
						),
						'*',
						IGNORE_MISSING
					)->completionstate;

					/*
					if($userId !== "7005")
					{
						continue;
						//  https://scratch.mit.edu/projects/389392616/
					}
					*/

					$assign_submission = $DB->get_record(
						"assign_submission",
						array(
							"id" => $submission->submission
						)
					);					

				?>
				<tr>
					<td class="text-center">
						<p><?php echo $submission->id ?></p>
						<div class="submission_timestamps">
							<span>created: <?php echo date('d/m/Y', $assign_submission->timecreated) ?></span>
							<span>modified: <?php echo date('d/m/Y', $assign_submission->timemodified) ?></span>
						</div>
					</td>
					<td>
						<b><?php echo $user->firstname .' '. $user->lastname ?></b> (<?php echo $userId ?>)
						<br>
						<small>
						<!--username: <?php echo $user->username ?>
						<br>
						email: --><?php echo $user->email ?>
						</small>
					</td>
					<td><?php echo $submission->assignment ?></td>
					<td><?php echo $submission->submission ?></td>
					<td>
						<div class="onlinetext">
							<!--<pre><code><?php echo $submission->onlinetext ?></code></pre>-->
							<?php echo html_entity_decode($submission->onlinetext) ?>
						</div>
					</td>
					<?php
						$gradeClass = 'grade-warning';
						if($grade < $maxGrade) {$gradeClass = 'grade-invalid';}
						if($grade == $maxGrade) {$gradeClass = 'grade-valid';}
						if($grade == NULL) {$gradeClass = 'grade-invalid';}
					?>					
					<td class="text-center grade-cell <?php echo $gradeClass ?>">
						<?php echo $grade ?> <small>/ <?php echo $maxGrade ?></small>
					</td>
					<td class="text-center">
						<i class="fa fa-<?php echo $completionState == 2 ? 'check' : 'times' ?>"></i>
						<!--<i><small>(<?php echo $completionState ?>)</small></i>-->
					</td>

					<!-- consegna già valutata positivamente -->
					<?php if($completionState == 2 && $grade == 100) {?>
						<?php $positive++; ?>
						<td colspan="11">Consegna già valutata correttamente</td>
					<?php } else { ?>

						<!-- consegna non ancora valutata o valutata negativamente -->
						<?php

							/* NEW */
							$url = $assignmentChecker->url_fetch($submission->onlinetext)[0];
							$submissionEvaluation = $assignmentChecker->evaluateSingleAssignment(
								$assignmentId,
								$submission->submission,
								$userId,
								$courseId,
								$courseModuleId
								);

							$overall = $submissionEvaluation["overall"];

							$nonPositive++;
							// valuto il primo url
							//$url = $assignmentChecker->url_fetch($submission->onlinetext)[0];
						?>
						<td>
							<?php
								if(!$submissionEvaluation["urlExists"]["required"]) { echo '-'; }
								else
								{
									echo '<small class="d-block">esistenza</small><div class="fetchedUrls">';
									echo '<div class="alert alert-'.($submissionEvaluation["urlExists"]["check"] ? 'success' : 'danger').'">'.$submissionEvaluation["urlExists"]["value"].'</div>';
									echo '</div>';
								}
								/*
								if(!$pluginInstanceUrlExists) { echo '-'; }
								else
								{
									echo '<small class="d-block">esistenza</small><div class="fetchedUrls">';
									$check = false;
									// prendo il primo
									//$fetched = $assignmentChecker->url_fetch($submission->onlinetext)[0];
									//foreach ($fetched as $key => $url) {
									//echo '<pre>';var_dump($assignmentChecker->url_exists($url));echo '</pre>';
									//continue;
										if($assignmentChecker->url_exists($url))
										{
											$check = true;
										}
										//break;
									//}
									echo '<div class="alert alert-'.($check ? 'success' : 'danger').'">'.$url.'</div>';
									echo '</div>';
									$overall = $overall && $check;
								}
								*/
							?>
						</td>
						<td>
							<?php
								if(!$submissionEvaluation["urlIsUnique"]["required"]) { echo '-'; }
								else
								{
									echo '<small class="d-block">unicità</small>';
									echo '<i class="fa fa-'.($submissionEvaluation["urlIsUnique"]["check"] ? 'check' : 'times').'"></i>';
								}
								/*							
								if(!$pluginInstanceUrlIsUnique) { echo '-'; }
								else
								{
									echo '<small class="d-block">unicità</small>';
									$check = false;
									//foreach ($fetched as $key => $url) {
										if ($assignmentChecker->url_is_unique($url, $submission->submission, $assignmentId, $userId))
										{
											$check = true; 
											//break;
										}
									//}
									echo '<i class="fa fa-'.($check ? 'check' : 'times').'"></i>';
									$overall = $overall && $check;
								}
								*/
							?>
						</td>
						<!--
						<td>
							<?php
							/*
								if(!$pluginInstanceUrlIsCorrect) { echo '-'; }
								else
								{
									echo '<small class="d-block">correttezza</small>';
									$check = false;
									foreach ($fetched as $key => $url) {
										if ($assignmentChecker->url_is_unique($url, $submission->submission, $assignmentId))
										{
											$check = true; 
											break;
										}
									}
									echo '<i class="fa fa-'.($check ? 'check' : 'times').'"></i>';
								}
								*/
							?>		
						</td>
						-->
						<td>
							<?php
								if(!$submissionEvaluation["urlContains"]["required"]) { echo '-'; }
								else
								{
									echo '<small class="d-block" style="white-space:nowrap;">contiene "<b>'.$submissionEvaluation["urlContains"]["value"].'</b>"</small>';
									echo '<i class="fa fa-'.($submissionEvaluation["urlContains"]["check"] ? 'check' : 'times').'"></i>';
								}							
								/*
								if(!$pluginInstanceUrlContains) { echo '-'; }
								else
								{
									echo '<small class="d-block" style="white-space:nowrap;">contiene "<b>'.$pluginInstanceUrlContains.'</b>"</small>';
									$check = false;
									//foreach ($fetched as $key => $url) {
										if ($assignmentChecker->url_contains($url, $pluginInstanceUrlContains))
										{
											$check = true; 
											//break;
										}
									//}
									echo '<i class="fa fa-'.($check ? 'check' : 'times').'"></i>';
									$overall = $overall && $check;
								}
								*/
							?>		
						</td>
						<td>
							<?php
								if(!$submissionEvaluation["urlStartsWith"]["required"]) { echo '-'; }
								else
								{
									echo '<small class="d-block" style="white-space:nowrap;">inizia per "<b>'.$submissionEvaluation["urlStartsWith"]["value"].'</b>"</small>';
									echo '<i class="fa fa-'.($submissionEvaluation["urlStartsWith"]["check"] ? 'check' : 'times').'"></i>';
								}
								/*
								if(!$pluginInstanceUrlStartsWith) { echo '-'; }
								else
								{
									echo '<small class="d-block" style="white-space:nowrap;">inizia per "<b>'.$pluginInstanceUrlStartsWith.'</b>"</small>';
									$check = false;
									//foreach ($fetched as $key => $url) {
										if ($assignmentChecker->url_starts_with($url, $pluginInstanceUrlStartsWith))
										{
											$check = true; 
											//break;
										}
									//}
									echo '<i class="fa fa-'.($check ? 'check' : 'times').'"></i>';
									$overall = $overall && $check;
								}
								*/
							?>		
						</td>
						
						<td>
							<?php
								if(!$submissionEvaluation["urlEndsWith"]["required"]) { echo '-'; }
								else
								{
									echo '<small class="d-block" style="white-space:nowrap;">termina per "<b>'.$submissionEvaluation["urlEndsWith"]["value"].'</b>"</small>';
									echo '<i class="fa fa-'.($submissionEvaluation["urlEndsWith"]["check"] ? 'check' : 'times').'"></i>';
								}							
								/*
								if(!$pluginInstanceUrlEndsWith) { echo '-'; }
								else
								{
									echo '<small class="d-block" style="white-space:nowrap;">termina per "<b>'.$pluginInstanceUrlEndsWith.'</b>"</small>';
									$check = false;
									//foreach ($fetched as $key => $url) {
										if ($assignmentChecker->url_ends_with($url, $pluginInstanceUrlEndsWith))
										{
											$check = true; 
											//break;
										}
									//}
									echo '<i class="fa fa-'.($check ? 'check' : 'times').'"></i>';
									$overall = $overall && $check;
								}
								*/
							?>
						</td>

						<td>
							<?php
								if(!$submissionEvaluation["urlMatchesRegex"]["required"]) { echo '-'; }
								else
								{
									echo '<i class="fa fa-'.($submissionEvaluation["urlMatchesRegex"]["check"] ? 'check' : 'times').'"></i>';
								}
								/*							
								if(!$pluginInstanceUrlUrlMatchesRegex) { echo '-'; }
								else
								{
									$check = false;
									if ($assignmentChecker->url_matches_regex($onlinetext, $pluginInstanceUrlUrlMatchesRegex))
									{
										$check = true; 
									}
									echo '<i class="fa fa-'.($check ? 'check' : 'times').'"></i>';
									$overall = $overall && $check;
								}
								*/
							?>
						</td>
						
						<td>
							<?php
								if(!$submissionEvaluation["minChar"]["required"]) { echo '-'; }
								else
								{
									echo '<i class="fa fa-'.($submissionEvaluation["minChar"]["check"] ? 'check' : 'times').'"></i>';
								}							
								/*
								if(!$pluginInstanceMinChar) { echo '-'; }
								else
								{
									$check = false;
									if ($assignmentChecker->url_char_count($onlinetext, $pluginInstanceMinChar, 0))
									{
										$check = true; 
									}
									echo '<i class="fa fa-'.($check ? 'check' : 'times').'"></i>';
									$overall = $overall && $check;
								}
								*/
							?>
						</td>
						<td>
							<?php
								if(!$submissionEvaluation["maxChar"]["required"]) { echo '-'; }
								else
								{
									echo '<i class="fa fa-'.($submissionEvaluation["maxChar"]["check"] ? 'check' : 'times').'"></i>';
								}							
								/*							
								if(!$pluginInstanceMaxChar) { echo '-'; }
								else
								{
									$check = false;
									if ($assignmentChecker->url_char_count($onlinetext, 0, $pluginInstanceMaxChar))
									{
										$check = true; 
									}
									echo '<i class="fa fa-'.($check ? 'check' : 'times').'"></i>';
									$overall = $overall && $check;
								}
								*/
							?>							
						</td>
						<td>
							<?php
								if(!$submissionEvaluation["minWords"]["required"]) { echo '-'; }
								else
								{
									echo '<i class="fa fa-'.($submissionEvaluation["minWords"]["check"] ? 'check' : 'times').'"></i>';
								}							
								/*														
								if(!$pluginInstanceMinWords) { echo '-'; }
								else
								{
									$check = false;
									if ($assignmentChecker->url_word_count($onlinetext, $pluginInstanceMinWords, 0))
									{
										$check = true; 
									}
									echo '<i class="fa fa-'.($check ? 'check' : 'times').'"></i>';
									$overall = $overall && $check;
								}
								*/
							?>								
						</td>
						<td>
							<?php
								if(!$submissionEvaluation["maxWords"]["required"]) { echo '-'; }
								else
								{
									echo '<i class="fa fa-'.($submissionEvaluation["maxWords"]["check"] ? 'check' : 'times').'"></i>';
								}							
								/*																					
								if(!$pluginInstanceMaxWords) { echo '-'; }
								else
								{
									$check = false;
									if ($assignmentChecker->url_word_count($onlinetext, 0, $pluginInstanceMaxWords))
									{
										$check = true; 
									}
									echo '<i class="fa fa-'.($check ? 'check' : 'times').'"></i>';
									$overall = $overall && $check;
								}
								*/
							?>	
						</td>

						<td class="text-center">
							<small>valutazione complessiva</small><br>
							<div class="alert alert-<?php echo ($overall ? 'success' : 'danger')?>">
								<i class="fa fa-<?php echo $overall ? 'check' : 'times' ?>"></i>
							</div>
						</td>

						<?php
							if($overall)
							{
								$nonPositiveToPositive++;
							}
							else
							{
								$nonPositiveToNegative++;
							}
							$item = array(
								"email" 		=> $user->email,
								"firstname" 	=> str_replace("'", "", $user->firstname),
								"lastname" 		=> str_replace("'", "", $user->lastname),
								"grade" 		=> $submissionEvaluation["grade"],//($overall ? GRADE_VALID : GRADE_ERROR),
								"completion" 	=> $submissionEvaluation["completion"]//($overall ? COMPLETION_STATE_VALID : COMPLETION_STATE_ERROR)
							)
						?>
						<input hidden name='item-<?php echo $userId ?>' value='<?php echo json_encode($item) ?>' />

					<?php } ?>
				</tr>
				<?php $total++; ?>
				<?php } ?>
			</tbody>
		</table>
		</div>

		<div class="resume-heading" style="flex-direction:column;">
			<div class="row">
				<div class="col-sm-12 col-md-6">
					<p>Consegne totali: <b><?php echo $total ?></b></p>
					<p>Consegne valutate positivamente: <b><?php echo $positive ?></b></p>
					<p>Consegne non valutate o valutate negativamente: <b><?php echo $nonPositive ?></b></p>
				</div>
				<div class="col-sm-12 col-md-6">
					<p>Consegne in attesa di valutazione positiva: <b><?php echo $nonPositiveToPositive ?></b></p>
					<p>Consegne in attesa di valutazione negativa: <b><?php echo $nonPositiveToNegative ?></b></p>
				</div>
			</div>
			<div class="col-sm-12">
				<?php 
					$timestamp = time();
					$author = json_encode(array(
						$USER->id,
						$USER->firstname,
						$USER->lastname,
						$USER->email				
					));
				?>
				<input hidden name='evaluation' value='1'>
				<input hidden name='type' value='assignment'>
				<input hidden name='courseModuleId' value='<?php echo $courseModuleId ?>'>
				<input hidden name='assignmentId' value='<?php echo $assignmentId ?>'>
				<input hidden name='author' value='<?php echo $author ?>'>
				<input hidden name='timestamp' value='<?php echo time() ?>'>
				<input hidden name='courseId' value='<?php echo $courseId ?>'>
				
				<div class="text-center">
					<br><br>
					<button class="btn btn-primary" type="submit">RIVALUTA TUTTI I PREGRESSI</button>
					<br><br>
				</div>
			</div>
		</div>

	</div>

</div>
</div>
</div>

</form>
</body>
</html>
