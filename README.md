# ASSIGNMENT CHECKER

Moodle plugin per la validazione degli url in online text assignment

## Installazione e configurazione

1. installare il plugin in moodle
2. aggiungere almeno un assignment ad un corso, in una posizione qualunque, impostando "testo online" come tipo di consegna ed impostando il completamento dell'attività con: "Lo studente deve ricevere una valutazione per completare l'attività"
3. aggiungere un'istanza di Assignment Checker allo stesso corso, in una posizione qualsiasi
4. nelle impostazioni di istanza di Assignment Checker, configurare:
  
  - l'assignment di cui si vogliono verificare gli url inseriti (obbligatorio)
  - le modalità di controllo degli url
  - la visibilità del plugin (Impostazioni comuni > Visibilità) per nasconderlo agli studenti

## Criteri di validazione

### Esistenza

Se questo controllo è selezionato nelle impostazioni di istanza del plugin, verrà verificata la risposta ad una http request: se la risorsa risponde con un http code uguale a 200 oppure 301 allora il controllo è considerato superato, altrimenti viene considerato fallito.

### Correttezza

Se questo controllo è selezionato nelle impostazioni di istanza del plugin, l'url sottomesso verrà considerato valido nel caso in cui sia verificata almeno una delle seguenti condizioni:
  1. Almeno un controllo tra "inizia con", "contiene" e "termina con" ha esito positivo
  2. L'espressione regolare ha esito positivo
  (Il controllo viene considerato positivo per i campi che vengono lasciati vuoti)

### Unicità

Se questo controllo è selezionato nelle impostazioni di istanza del plugin, l'url viene considerato unico se non viene trovato lo stesso url nei record della tabella mdl_assignsubmission_onlinetext per quello specifico assignment.

## Valutazione e completamenti

- a prescindere dall'esito della validazione, l'assignment viene completato inserendo o aggiornando un record nella tabella: la mdl_course_modules_completion
- a seconda dell'esito della valutazione, nel record della tabella mdl_course_modules_completion viene inserito uno stato di completamento opportuno (valid / error), in modo da colorare la checkbox dell'interfaccia utente opportunamente.
- allo studente viene SEMPRE assegnata una valutazione per l'attività in esame: la valutazione è pari a 100 nel caso in cui l'url sia valido, mentre è pari a zero in caso contrario; la valutazione viene attribuita inserendo o aggiornando un record nella tabella: mdl_assign_grades
