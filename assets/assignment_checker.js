document.addEventListener("DOMContentLoaded", function(event) {
	var id_urliscorrect = document.getElementById("id_urliscorrect");
	if (!id_urliscorrect.checked) { document.body.classList = document.body.classList + " correctness-unchecked";}
	id_urliscorrect.addEventListener("change",function(){
		document.body.classList.toggle('correctness-unchecked');
	});

	//form check
	var minchar  = document.getElementById("id_minchar"),
		maxchar  = document.getElementById("id_maxchar"),
		minwords = document.getElementById("id_minwords"),
		maxwords = document.getElementById("id_maxwords"),
		assignmentid = document.getElementById("id_assignmentid"),
		workshopid = document.getElementById("id_workshopid"),
		applyall = document.getElementById('fitem_id_button_applytoall')
		;

	//console.log(assignmentid);
	//console.log(workshopid);

	minchar.setAttribute("type","number");
	minchar.setAttribute("min",0);
	maxchar.setAttribute("type","number");
	minwords.setAttribute("type","number");
	minwords.setAttribute("min",0);
	maxwords.setAttribute("type","number");

	minchar.addEventListener("change",function(e){
		var max = maxchar.value,
			min = parseInt(this.value)
			;
		if (max == 0) return;
		if (max < min | max == min)
		{
			var newMax = Math.max(0,min+1);
			maxchar.value = newMax;
		}
	});
	maxchar.addEventListener("change",function(e){
		var min = minchar.value,
		max = parseInt(this.value)
		;
		if (min == 0) return;
		if (min > max | min == max)
		{
			var newMin = Math.max(0,max-1);
			minchar.value = newMin;
		}
	});
	minwords.addEventListener("change",function(e){
		var max = maxwords.value,
			min = parseInt(this.value)
			;
		if (max == 0) return;
		if (max < min | max == min)
		{
			var newMax = Math.max(0,min+1);
			maxwords.value = newMax;
		}
	});
	maxwords.addEventListener("change",function(e){
		var min = minwords.value,
		max = parseInt(this.value)
		;
		if (min == 0) return;
		if (min > max | min == max)
		{
			var newMin = Math.max(0,max-1);
			minwords.value = newMin;
		}
	});

/*
	if (workshopid.value !== 0)
	{
		applyall.style.opacity = 0;
	}
*/
	workshopid.addEventListener("change",function(e){
		assignmentid.value = 0;
		//applyall.style.opacity = 0;
	});
				
	assignmentid.addEventListener("change",function(e){
		workshopid.value = 0;
		//applyall.style.opacity = 1;
	});
				
	document.getElementById("id_button_applytoall").addEventListener("click",function(){
		
		var value = document.getElementsByName("input_applytoall")[0].value,
			box = document.getElementById('fitem_id_button_applytoall'),
			xmlhttp = new XMLHttpRequest()
			;

		if(workshopid.value)
		{
			value += "&workshop=1&workshopid="+workshopid.value;
		}

		if (!value) return;

		console.log(value);

		box.classList.add("wait");

		xmlhttp.onreadystatechange = function(data) {
			if (xmlhttp.readyState == XMLHttpRequest.DONE) {
				console.log(xmlhttp.responseText);
			   	if (xmlhttp.status == 200) {
					box.classList.remove("wait");
					box.classList.add("done");
					window.setTimeout(function(){
						box.classList.remove("done");
					},2000);
				}
				else
				{
					box.classList.remove("wait");
					box.classList.add("bad");
					window.setTimeout(function(){
						box.classList.remove("bad");
					},2000);
			  	}
			}
		};
		
		xmlhttp.open("GET", value, true);
		xmlhttp.send();

	});
});