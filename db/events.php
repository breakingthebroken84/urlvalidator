<?php

    defined('MOODLE_INTERNAL') || die();

    $observers = array(
        array(
            'eventname' => 'mod_assign\event\assessable_submitted',
            'callback' => 'mod_assignmentchecker_observer::assignment_submitted'
        ),
        array(
            'eventname' => 'mod_workshop\event\submission_created',
            'callback' => 'mod_assignmentchecker_observer::workshop_submitted'
        ),
        array(
            'eventname' => 'mod_workshop\event\submission_updated',
            'callback' => 'mod_assignmentchecker_observer::workshop_submitted'
        )
    );