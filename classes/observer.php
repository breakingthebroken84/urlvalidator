<?php

require_once(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php');
require_once(dirname(dirname(__FILE__)).'/lib.php');

define(DEBUG, true);
define(COMPLETION_STATE_ERROR, "3");
define(COMPLETION_STATE_VALID, "2");
define(GRADE_ERROR, 0);
define(GRADE_VALID, 100);

class mod_assignmentchecker_observer
{
	public static function workshop_submitted($event)
	{
		global $CFG;
		global $DB;

		$eventData = $event->get_data();
		$submissionId = $eventData["objectid"];
		$userId = $eventData["userid"];
		$courseId = $eventData["courseid"];
		$courseModuleId = $eventData["contextinstanceid"];

		$workshopId = $DB->get_record(
			'workshop_submissions',
			array(
				"id" 	=> $submissionId
			),
			'*',
			IGNORE_MISSING
		)->workshopid;

		//if($userId == "7005")
		//{
			$submissionEvaluation = self::evaluateSingleWorkshop(
				$workshopId,
				$submissionId,//OK
				$userId,//OK
				$courseId,//OK
				$courseModuleId//OK
			);

			$timestamp = time();
			$completion = $submissionEvaluation["completion"];
			$grade = $submissionEvaluation["grade"];

			self::set_completed_course_module($userId, $courseModuleId, $completion, $timestamp);
			self::set_workshop_grade($grade, $userId, $workshopId, $userId, $timestamp);			
			purge_all_caches();
		//}
	}

	public static function assignment_submitted(mod_assign\event\assessable_submitted $event)
	{
		global $CFG;
		global $DB;

		$eventData = $event->get_data();
		$submissionId = $eventData["objectid"];
		$userId = $eventData["userid"];
		$courseId = $eventData["courseid"];
		$courseModuleId = $eventData["contextinstanceid"];

		/*
		if($userId == "7005") return;
		if($userId == 7005) return;
		*/

		$assignmentId = $DB->get_record(
			'assignsubmission_onlinetext',
			array(
				"submission" 	=> $submissionId
			),
			'*',
			IGNORE_MISSING
		)->assignment;

		$submission = $DB->get_record(
			"assignsubmission_onlinetext",
			array(
				"assignment" => $assignmentId,
				"submission" => $submissionId
			)
		);
		
		//if($userId == 7005)
		//{
			$submissionEvaluation = self::evaluateSingleAssignment(
				$assignmentId,
				$submissionId,
				$userId,
				$courseId,
				$courseModuleId
			);

			$timestamp = time();
			$completion = $submissionEvaluation["completion"];
			$grade = $submissionEvaluation["grade"];

			self::set_completed_course_module($userId, $courseModuleId, $completion, $timestamp);
			self::set_assignment_grade($grade, $userId, $assignmentId, $timestamp);
			purge_all_caches();
		//}
    }

    public static function set_completed_course_module($userId, $moduleId, $state, $timestamp)
    {
    	global $DB;

    		/*
    		self::xxx($moduleId);
    		self::xxx($userId);
    		self::xxx($state);
    		self::xxx($timestamp);
    		*/    	

		$record = $DB->get_record(
			'course_modules_completion',
			array(
				"coursemoduleid" 	=> $moduleId,
				"userid" 			=> $userId
			),
			'*',
			IGNORE_MISSING
		);

		$id = $record->id;

    	if ($id !== null)
    	{
    		$DB->update_record(
	    		'course_modules_completion',
				array(
					"id"				=> $id,
					"coursemoduleid" 	=> $moduleId,
					"userid" 			=> $userId,
					"completionstate" 	=> $state,
					"viewed" 			=> 0,
					"timemodified" 		=> $timestamp
				),
    			false
    		);
    	}
    	else
    	{
			$id = $DB->insert_record(
				'course_modules_completion',
				array(
					"coursemoduleid" 	=> $moduleId,
					"userid" 			=> $userId,
					"completionstate" 	=> $state,
					"viewed" 			=> 0,
					"timemodified" 		=> $timestamp
				),
				true,	//return id
				false 	//docs just says this parameter is unused (??)
			);

		}

		if ($id == false)
		{
			return false;
		}

    	return true;
    }

    public static function set_assignment_grade($grade, $userId, $assignmentId, $timestamp)
    {
    	global $DB;

		$record = $DB->get_record(
			'assign_grades',
			array(
				"userid" 		=> $userId,
				"assignment" 	=> $assignmentId,
			),
			'*',
			IGNORE_MISSING
		);

		$id = $record->id;

    	if ($id !== null)
    	{
    		$DB->update_record(
	    		'assign_grades',
				array(
					"id"			=> $id,
					"userid" 		=> $userId,
					//"grader"		=> $userId,
					"assignment" 	=> $assignmentId,
					//"timecreated" 	=> $timestamp,
					"timemodified" 	=> $timestamp,
					"grade" 		=> $grade
				),
    			false
    		);
    	}
    	else    	
    	{
			$id = $DB->insert_record(
				'assign_grades',
				array(
					"userid" 		=> $userId,
					//"grader"		=> $userId,
					"assignment" 	=> $assignmentId,
					"timecreated" 	=> $timestamp,
					"timemodified" 	=> $timestamp,
					"grade" 		=> $grade
				),
				true,	//return id
				false 	//docs just says this parameter is unused (??)
			);
		}

		if ($id == false)
		{
			return false;
		}

    	return true;
    }

    public static function set_workshop_grade($grade, $userId, $workshopId, $overById, $timestamp)
    {

    	global $DB;

    	//echo $grade.'<br>';
    	//echo $userId.'<br>';
    	//echo $workshopId.'<br>';
    	//echo $overById.'<br>';
    	//echo $timestamp.'<br>';

		$gradeOver = $DB->get_record(
			'workshop_submissions',
			array(
				"authorid" 		=> $userId,
				"workshopid" 	=> $workshopId,
			),
			'*',
			IGNORE_MISSING
		);

		if($gradeOver->id)
		{
			$DB->update_record(
	    		'workshop_submissions',
				array(
					"id"			=> $gradeOver->id,
					"authorid" 		=> $userId,
					"workshopid" 	=> $workshopId,
					"grade"			=> $grade,
					"gradeover"		=> $grade,
					"gradeoverby"	=> $overById,
					"timegraded"	=> time()
				),
				false
			);
		}

		else
		{
			$DB->insert_record(
	    		'workshop_submissions',
				array(
					"id"			=> $gradeOver->id,
					"authorid" 		=> $userId,
					"workshopid" 	=> $workshopId,
					"grade"			=> $grade,
					"gradeover"		=> $grade,
					"gradeoverby"	=> $overById,
					"timegraded"	=> time()
				),
				false
			);
		} 	
    }

    public static function url_fetch($onlinetext)
    {
		$urls = explode('http',$onlinetext);
		$lastUrl = '';
		$fetched = [];
		foreach ($urls as $key => $url) 
		{
			$url = explode('"',$url)[0];
			$url = explode('>',$url)[0];
			$url = explode("'",$url)[0];
			$url = explode('?',$url)[0];
			$url = explode('#',$url)[0];
			$url = explode(',',$url)[0];
			$url = explode(';',$url)[0];
			$url = explode(' ',$url)[0];
			$url = preg_replace('/\s+/', ' ', $url);
			$url = preg_replace('/\s+/', '&nbsp;', $url);
			$url = preg_replace('/\s+/', '\xc2\xa0', $url);
			
			$url = self::url_prepare($url, array("remove_spaces","remove_newlines","strip_tags"));

			//blacklisted URLS
			if (strpos($url, 'scratch.mit.edu/users')) continue;
			if (strpos($url, 'scratch.mit.edu/mystuff')) continue;
			if (strpos($url, 'mooc.uniurb.it')) continue;
			if (strpos($url, 'code.org/congrats')) continue;

			//duplicated or invalid url
			if (strcmp($url, $lastUrl) == 0) continue;
			if ($url == $lastUrl) continue;
			if (strlen($url) < 5) continue;
			
			$lastUrl = $url;
			$url = 'http'.$url;
			//echo '<p style="background:rgba(100,100,100,0.1);color:green;white-space:nowrap;">'.urlencode($url).'</p>';
			//echo '<p style="background:rgba(100,100,100,0.1);color:green;white-space:nowrap;">'.$url.'</p>';
			array_push($fetched, $url);
		}
		return $fetched;
    }

    public static function url_prepare($url, $conditions)
    {

    	// remove newlines
    	$url = trim(preg_replace('/\s+/', ' ', $url));
    	
    	// remove tags
    	$url = preg_replace('/<[^>]*>/', '', $url);

    	
    	if(in_array("remove_spaces", $conditions))
    	{
    		
    		$url = str_replace("&nbsp;", "", $url);
    		$url = str_replace("\xc2\xa0", "", $url);
    		$url = str_replace(" ", "", $url);
    		$url = preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $url);
    		$url = preg_replace('/[\x00-\x1F\x7F]/', '', $url);
    		$url = preg_replace('/[\x00-\x1F\x7F]/u', '', $url);
    		$url = preg_replace('/[^!-~]+/', " ", $url);
    		$url = preg_replace('/[[:^print:]]/', '', $url);


    		// caso in cui viene linkato l'editor per errore: https://scratch.mit.edu/projects/139417221/editor
    		$url = str_replace("/editor", "/", $url);
    		// caso in cui viene linkato il progetto fullcsreen per errore: https://scratch.mit.edu/projects/104694408/fullscreen
    		$url = str_replace("/fullscreen", "/", $url);
    		// caso in cui viene embeddato un iframe per errore: https://scratch.mit.edu/projects/310925034/embed
    		$url = str_replace("/embed", "/", $url);
			// caso in cui viene modificato l'html per errore: https://scratch.mit.edu/projects/320053872/span
    		$url = rtrim($url,"/span");
    		//$url = rtrim($url,"/p");
    		
    		//$url = rtrim($url,"/strong");
    		//$url = rtrim($url,"/)b");
    		$url = str_replace("(", "", $url);
    		$url = str_replace(")", "", $url);
    		

    		// rimuovo caratteri non printable interni
    		$url = urlencode($url);
    		$url = str_replace("%3C", "", $url);
    		//$url = str_replace("%2Fa", "%2F", $url);
    		//$url = str_replace("%2Fbr", "%2F", $url);
    		//$url = str_replace("%2Fb", "%2F", $url);
    		//$url = str_replace("%2Fb", "%2F", $url);
    		$url = str_replace("br", "", $url);
    		
    		//rimuovo tutti i caratteri non printable dopo il trailing slash
    		//$pos = strrpos($url,"%2F");
    		//if($pos) $url = substr($url, 0, ($pos));
    		$url = urldecode($url);
    		$url = rtrim($url,"/");
    	}

    	if(in_array("remove_newlines", $conditions))
    	{
    		$url = trim(preg_replace('/\s+/', ' ', $url));
    	}

    	if(in_array("strip_tags", $conditions))
    	{
    		$url = preg_replace('/<[^>]*>/', '', $url);
    	}

    	$url = preg_replace('/\s+/', ' ', $url);
    	
    	return trim($url);
    }

	public static function url_exists($url)
	{
	    $length = strlen($url);
	    if ($length == 0) {
	        return false;
	    }

	    /*
	    if(strpos($url, "code.org") && strpos($url, "jpg"))
	    {
	    	$url = str_replace(".jpg","",$url);
	    }
	    */

	    if(strpos($url, "code.org") && strpos($url, "/edit"))
	    {
	    	return false;
	    }

	    // eccezioni code.org
	    if(strpos($url, "scratch.mit.edu"))
	    {
	    	if(strpos($url, "editor")) return false;
	    	if(strpos($url, "users")) return false;
	    }

		$headers = get_headers($url);

		$exists = preg_match("#^HTTP/.*\s+[(200|301)]+\s#i", $headers[0]);
		if($exists !== 1){
		    return false;
		}
		

		if(strpos($url, "code.org") && strpos($url, "/edit"))
		{
			// start hack to get rid of code.org redirect to default certificate
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$returned = curl_exec($ch);
			curl_close ($ch);
			// if the body response contains the default code.org certificate (ex: /images/hour_of_code_certificate.jpg), return false
			if(preg_match('/\/images\/[\s\S]*_certificate.jpg/', $returned))
			{
		    	return false;
			}
			// end hack to get rid of code.org redirect to default certificate
		}

	    return true;
	}

	public static function url_is_unique($url, $submissionId, $assignmentId, $userId)
	{
    	/*
        . "FROM mdl_assignsubmission_onlinetext "
        . "WHERE assignment = {$assignmentid} "
        . "AND submission <> {$submissionid} "
        . "AND onlinetext LIKE '%$url%'";
		*/

	    global $DB;
	    
		//$select = "assignment = {$assignmentId} AND submission <> {$submissionId} AND onlinetext LIKE '%{$url}%'"; 
		$select = "assignment = {$assignmentId} AND submission <> {$submissionId} AND onlinetext LIKE '%{$url}%'"; 
	    $result = $DB->get_records_select(
			"assignsubmission_onlinetext",
			$select
		);

		if(count($result) == 0)
		{
			return true;
		}

		return false;

		/*
		foreach ($result as $key => $submission) {
			$oldUserId = $DB->get_record(
				"assign_submission",
				array(
					"id" => $submissionId
				)
			)->userid;

			if($oldUserId !== $userId)
			{
				return false;
			}
		}

		return true;
		*/
	}

	public static function url_starts_with($subject, $needle)
	{
		$length = strlen($needle);
	    if ($length == 0) {
	        return false;
	    }

	    // code.org exception
	    if(strpos($needle, "code.org") && strpos($needle, "certificate"))
	    {
	    	if(strpos($subject, "code.org/printcertificate")) {return true;}
	    	if(strpos($subject, "code.org/v2/hoc/certificate")) {return true;}
	    	if(strpos($subject, "code.org/certificates")) {return true;}
	    	if(strpos($subject, "code.org/api/hour/certificate")) {return true;}
	    }

		return (substr($subject, 0, $length) === $needle);
	}

	public static function url_ends_with($subject, $needle)
	{
	    $length = strlen($needle);
	    if ($length == 0) {
	        return false;
	    }

	    // code.org exception
	    if(strpos($needle, "code.org") && strpos($needle, "certificate"))
	    {
	    	if(strpos($subject, "code.org/printcertificate")) {return true;}
	    	if(strpos($subject, "code.org/v2/hoc/certificate")) {return true;}
	    	if(strpos($subject, "code.org/certificates")) {return true;}
	    	if(strpos($subject, "code.org/api/hour/certificate")) {return true;}
	    }

	    return (substr($subject, -$length) === $needle);
	}

	public static function url_contains($subject, $needle)
	{
	    $length = strlen($needle);
	    if ($length == 0) {
	        return false;
	    }

	    // code.org exception
	    if(strpos($needle, "code.org") && strpos($needle, "certificate"))
	    {
	    	if(strpos($subject, "code.org/printcertificate")) {return true;}
	    	if(strpos($subject, "code.org/v2/hoc/certificate")) {return true;}
	    	if(strpos($subject, "code.org/certificates")) {return true;}
	    	if(strpos($subject, "code.org/api/hour/certificate")) {return true;}
	    }

	    if(strpos($subject, $needle) !== false)
    	{
    		return true;
    	}
		return false;
	}

	public static function prepare_text_before_count($text)
	{
		$text = trim($text);
		$text = strip_tags(html_entity_decode($text));
		$text = preg_replace('/[\x00-\x1F\x7F]/u', '', $text);
		return $text;
	}

	public static function url_word_count($subject, $min, $max)
	{
		//$subject = self::url_prepare($subject, array("strip_tags"));
		//$subject = trim($subject);
		$subject = self::prepare_text_before_count($subject);

	    $length = str_word_count($subject);
	    if ($length == 0) {
	        return false;
		}

		if (($min==0) & ($max==0))
		{
			return false;
		}

		if (($min=='') & ($max==''))
		{
			return false;
		}

		if ((!$min) & (!$max))
		{
			return false;
		}

		if($min != 0 & $length < $min)
		{
			return false;
		}

		if($max != 0 & $length > $max)
		{
			return false;
		}

	    return true;
	}

	public static function url_char_count($subject, $min, $max)
	{
		//$subject = self::url_prepare($subject, array("strip_tags"));
		//$subject = trim($subject);
		$subject = self::prepare_text_before_count($subject);

	    $length = strlen($subject);

	    if ($length == 0) {
	        return false;
		}
		
		if (($min==0) & ($max==0))
		{
			return false;
		}

		if (($min=='') & ($max==''))
		{
			return false;
		}

		if ((!$min) & (!$max))
		{
			return false;
		}

		if($min != 0 & $length < $min)
		{
			return false;
		}

		if($max != 0 & $length > $max)
		{
			return false;
		}

	    return true;
	}
	// /^https?:\/\/code.org\/printcertificate\//
	public static function url_matches_regex($subject, $expression)
	{
		// eccezione nel caso di tag html link
		if(self::url_contains($subject, '&lt;a href='))
		{
			$link = $subject;
			$link = str_replace("&lt;", "<", $link);
			$link = str_replace("&gt;", ">", $link);
			$dom = new DOMDocument;
			@$dom->loadHTML($link);
			$tag = $dom->getElementsByTagName('a');
			if(count($tag))
			{
				$check = preg_match($expression,$tag[0]->getAttribute('href'));
				if($check)
				{
					return true;
				}
			}
		}

		$subject = self::url_prepare($subject, array("strip_tags"));
		$subject = trim($subject);
		$length = strlen($expression);
		if ($length == 0) {
			return false;
		}

		$subject = htmlentities($subject, null, 'utf-8');
		$subject = trim(preg_replace('/\s\s+/', ' ', $subject));
		//$subject = "https://code.org/printcertificate/_1_c5d602291357b74661cddb6aef00e8ae";
		$expression = htmlentities($expression, null, 'utf-8');
		$expression = (mb_substr($expression,-1) == "/") ? $expression : $expression."/";
		$expression = ($expression[0] == "/") ? $expression : "/".$expression;
		$check = preg_match($expression, $subject);

	    return $check;		
	}

    public static function xxx($foo)
    {
    	if (DEBUG)
    	{
	    	echo '<pre>';
	    	var_dump($foo);
	    	echo '</pre>';
	    }
	    return;
    }


    /*NEW*/
	public static function evaluateSingleAssignment(
		$assignmentId,
		$submissionId,
		$userId,
		$courseId,
		$courseModuleId
		)
	{
		global $DB;

		$pluginInstance = $DB->get_record(
		    "assignmentchecker",
		    array(
		       "course" => $courseId,
		       "assignmentid" => $assignmentId
		    )
		);

		$assignment = $DB->get_record(
		    "assign",
		    array(
		        "id" => $assignmentId
		    )
		);
		
		$submission = $DB->get_record(
			"assignsubmission_onlinetext",
			array(
				//"id" => $submissionId,
				"assignment" => $assignmentId,
				"submission" => $submissionId				
			)
		);

		$onlinetext = $submission->onlinetext;
		$maxGrade = $assignment->grade;

		$pluginInstanceName = $pluginInstance->name;
		$pluginInstanceUrlExists = $pluginInstance->urlexists;
		$pluginInstanceUrlIsUnique = $pluginInstance->urlisunique;
		$pluginInstanceUrlIsCorrect = $pluginInstance->urliscorrect;
		$pluginInstanceUrlContains = $pluginInstance->urlcontains;
		$pluginInstanceUrlStartsWith = $pluginInstance->urlstartswith;
		$pluginInstanceUrlEndsWith = $pluginInstance->urlendswith;
		$pluginInstanceUrlMatchesRegex = $pluginInstance->urlmatchesregex;
		$pluginInstanceMinChar = $pluginInstance->minchar;
		$pluginInstanceMaxChar = $pluginInstance->maxchar;
		$pluginInstanceMinWords = $pluginInstance->minwords;
		$pluginInstanceMaxWords = $pluginInstance->maxwords;

		$fieldCheck = array(
			"urlExists" => array(
					"required" => false,
					"check" => false,
					"value" => ""
				),
			"urlIsUnique" => array(
					"required" => false,
					"check" => false,
					"value" => ""
				),
			"urlContains" => array(
					"required" => false,
					"check" => false,
					"value" => ""
				),
			"urlStartsWith" => array(
					"required" => false,
					"check" => false,
					"value" => ""
				),
			"urlEndsWith" => array(
					"required" => false,
					"check" => false,
					"value" => ""
				),
			"urlMatchesRegex" => array(
					"required" => false,
					"check" => false,
					"value" => ""
				),
			"minChar" => array(
					"required" => false,
					"check" => false,
					"value" => ""
				),
			"maxChar" => array(
					"required" => false,
					"check" => false,
					"value" => ""
				),
			"minWords" => array(
					"required" => false,
					"check" => false,
					"value" => ""
				),
			"maxWords" => array(
					"required" => false,
					"check" => false,
					"value" => ""
				),
			"overall" => false
			//"urlIsCorrect" => -1, // la correttezza è un aggregato dei controlli successivi
		);
		$overall = true;

		$url = self::url_fetch($submission->onlinetext)[0];

		if($pluginInstanceUrlExists)
		{
			$fieldCheck["urlExists"]["required"] = true;
			$fieldCheck["urlExists"]["value"] = $url;
			if(self::url_exists($url))
			{
				$fieldCheck["urlExists"]["check"] = true;
			}
			$overall = $overall && $fieldCheck["urlExists"]["check"];
		}

		if($pluginInstanceUrlIsUnique)
		{
			$fieldCheck["urlIsUnique"]["required"] = true;
			$fieldCheck["urlIsUnique"]["value"] = $pluginInstanceUrlIsUnique;
			//if(self::url_is_unique($url, $submission->submission, $assignmentId, $userId))
			if(self::url_is_unique($url, $submissionId, $assignmentId, $userId))
			{
				$fieldCheck["urlIsUnique"]["check"] = true;
			}
			$overall = $overall && $fieldCheck["urlIsUnique"]["check"];
		}

		if($pluginInstanceUrlContains)
		{
			$fieldCheck["urlContains"]["required"] = true;
			$fieldCheck["urlContains"]["value"] = $pluginInstanceUrlContains;
			if(self::url_contains($url, $pluginInstanceUrlContains))
			{
				$fieldCheck["urlContains"]["check"] = true;
			}
			$overall = $overall && $fieldCheck["urlContains"]["check"];
		}

		if($pluginInstanceUrlStartsWith)
		{
			$fieldCheck["urlStartsWith"]["required"] = true;
			$fieldCheck["urlStartsWith"]["value"] = $pluginInstanceUrlStartsWith;
			if(self::url_starts_with($url, $pluginInstanceUrlStartsWith))
			{
				$fieldCheck["urlStartsWith"]["check"] = true;
			}
			$overall = $overall && $fieldCheck["urlStartsWith"]["check"];
		}

		if($pluginInstanceUrlEndsWith)
		{
			$fieldCheck["urlEndsWith"]["required"] = true;
			$fieldCheck["urlEndsWith"]["value"] = $pluginInstanceUrlEndsWith;
			if(self::url_ends_with($url, $pluginInstanceUrlEndsWith))
			{
				$fieldCheck["urlEndsWith"]["check"] = true;
			}
			$overall = $overall && $fieldCheck["urlEndsWith"]["check"];
		}

		if($pluginInstanceUrlMatchesRegex)
		{
			$fieldCheck["urlMatchesRegex"]["required"] = true;
			$fieldCheck["urlMatchesRegex"]["value"] = $pluginInstanceUrlMatchesRegex;
			if(self::url_matches_regex($onlinetext, $pluginInstanceUrlMatchesRegex))
			{
				$fieldCheck["urlMatchesRegex"]["check"] = true;
			}
			$overall = $overall && $fieldCheck["urlMatchesRegex"]["check"];
		}

		if($pluginInstanceMinChar)
		{
			$fieldCheck["minChar"]["required"] = true;
			$fieldCheck["minChar"]["value"] = $pluginInstanceMinChar;
			if(self::url_char_count($onlinetext, $pluginInstanceMinChar, 0))
			{
				$fieldCheck["minChar"]["check"] = true;
			}
			$overall = $overall && $fieldCheck["minChar"]["check"];
		}

		if($pluginInstanceMaxChar)
		{
			$fieldCheck["maxChar"]["required"] = true;
			$fieldCheck["maxChar"]["value"] = $pluginInstanceMaxChar;
			if(self::url_char_count($onlinetext, 0, $pluginInstanceMaxChar))
			{
				$fieldCheck["maxChar"]["check"] = true;
			}
			$overall = $overall && $fieldCheck["maxChar"]["check"];
		}

		if($pluginInstanceMinWords)
		{
			$fieldCheck["minWords"]["required"] = true;
			$fieldCheck["minWords"]["value"] = $pluginInstanceMinWords;
			if(self::url_word_count($onlinetext, $pluginInstanceMinWords, 0))
			{
				$fieldCheck["minWords"]["check"] = true;
			}
			$overall = $overall && $fieldCheck["minWords"]["check"];
		}

		if($pluginInstanceMaxWords)
		{
			$fieldCheck["maxWords"]["required"] = true;
			$fieldCheck["maxWords"]["value"] = $pluginInstanceMaxWords;
			if(self::url_word_count($onlinetext, 0, $pluginInstanceMaxWords))
			{
				$fieldCheck["maxWords"]["check"] = true;
			}
			$overall = $overall && $fieldCheck["maxWords"]["check"];
		}

		$fieldCheck["overall"] = $overall;
		$fieldCheck["grade"] = ($overall ? GRADE_VALID : GRADE_ERROR);
		$fieldCheck["completion"] = ($overall ? COMPLETION_STATE_VALID : COMPLETION_STATE_ERROR);

		return $fieldCheck;

	}

	public static function evaluateSingleWorkshop(
		$workshopId,
		$submissionId,
		$userId,
		$courseId,
		$courseModuleId
		)
	{
		global $DB;

		$pluginInstance = $DB->get_record(
		    "assignmentchecker",
		    array(
		       "course" => $courseId,
		       "workshopid" => $workshopId
		    )
		);

		$assign = $DB->get_record(
		    "assign",
		    array(
		        "id" => $workshopId
		    )
		);
		
		$submission = $DB->get_record(
			"workshop_submissions",
			array(
				//"id" => 3029,
				"workshopid" => $workshopId,
				"authorid" => $userId,
				"example" => 0
			)
		);

		$workshop = $DB->get_record(
		    "workshop",
		    array(
		        "id" => $workshopId
		    )
		);
		$maxGrade = floatval($workshop->grade);
		$onlinetext = $submission->content;
		//$maxGrade = $assign->grade;

		$pluginInstanceName = $pluginInstance->name;
		$pluginInstanceUrlExists = $pluginInstance->urlexists;
		$pluginInstanceUrlIsUnique = $pluginInstance->urlisunique;
		$pluginInstanceUrlIsCorrect = $pluginInstance->urliscorrect;
		$pluginInstanceUrlContains = $pluginInstance->urlcontains;
		$pluginInstanceUrlStartsWith = $pluginInstance->urlstartswith;
		$pluginInstanceUrlEndsWith = $pluginInstance->urlendswith;
		$pluginInstanceUrlMatchesRegex = $pluginInstance->urlmatchesregex;
		$pluginInstanceMinChar = $pluginInstance->minchar;
		$pluginInstanceMaxChar = $pluginInstance->maxchar;
		$pluginInstanceMinWords = $pluginInstance->minwords;
		$pluginInstanceMaxWords = $pluginInstance->maxwords;

		$fieldCheck = array(
			"urlExists" => array(
					"required" => false,
					"check" => false,
					"value" => ""
				),
			"urlIsUnique" => array(
					"required" => false,
					"check" => false,
					"value" => ""
				),
			"urlContains" => array(
					"required" => false,
					"check" => false,
					"value" => ""
				),
			"urlStartsWith" => array(
					"required" => false,
					"check" => false,
					"value" => ""
				),
			"urlEndsWith" => array(
					"required" => false,
					"check" => false,
					"value" => ""
				),
			"urlMatchesRegex" => array(
					"required" => false,
					"check" => false,
					"value" => ""
				),
			"minChar" => array(
					"required" => false,
					"check" => false,
					"value" => ""
				),
			"maxChar" => array(
					"required" => false,
					"check" => false,
					"value" => ""
				),
			"minWords" => array(
					"required" => false,
					"check" => false,
					"value" => ""
				),
			"maxWords" => array(
					"required" => false,
					"check" => false,
					"value" => ""
				),
			"overall" => false
			//"urlIsCorrect" => -1, // la correttezza è un aggregato dei controlli successivi
		);
		$overall = true;

		$url = self::url_fetch($submission->onlinetext)[0];

		if($pluginInstanceUrlExists)
		{
			$fieldCheck["urlExists"]["required"] = true;
			$fieldCheck["urlExists"]["value"] = $url;
			if(self::url_exists($url))
			{
				$fieldCheck["urlExists"]["check"] = true;
			}
			$overall = $overall && $fieldCheck["urlExists"]["check"];
		}

		if($pluginInstanceUrlIsUnique)
		{
			$fieldCheck["urlIsUnique"]["required"] = true;
			$fieldCheck["urlIsUnique"]["value"] = $pluginInstanceUrlIsUnique;
			//if(self::url_is_unique($url, $submission->submission, $assignmentId, $userId))
			if(self::url_is_unique($url, $submissionId, $assignmentId, $userId))
			{
				$fieldCheck["urlIsUnique"]["check"] = true;
			}
			$overall = $overall && $fieldCheck["urlIsUnique"]["check"];
		}

		if($pluginInstanceUrlContains)
		{
			$fieldCheck["urlContains"]["required"] = true;
			$fieldCheck["urlContains"]["value"] = $pluginInstanceUrlContains;
			if(self::url_contains($url, $pluginInstanceUrlContains))
			{
				$fieldCheck["urlContains"]["check"] = true;
			}
			$overall = $overall && $fieldCheck["urlContains"]["check"];
		}

		if($pluginInstanceUrlStartsWith)
		{
			$fieldCheck["urlStartsWith"]["required"] = true;
			$fieldCheck["urlStartsWith"]["value"] = $pluginInstanceUrlStartsWith;
			if(self::url_starts_with($url, $pluginInstanceUrlStartsWith))
			{
				$fieldCheck["urlStartsWith"]["check"] = true;
			}
			$overall = $overall && $fieldCheck["urlStartsWith"]["check"];
		}

		if($pluginInstanceUrlEndsWith)
		{
			$fieldCheck["urlEndsWith"]["required"] = true;
			$fieldCheck["urlEndsWith"]["value"] = $pluginInstanceUrlEndsWith;
			if(self::url_ends_with($url, $pluginInstanceUrlEndsWith))
			{
				$fieldCheck["urlEndsWith"]["check"] = true;
			}
			$overall = $overall && $fieldCheck["urlEndsWith"]["check"];
		}

		if($pluginInstanceUrlMatchesRegex)
		{
			$fieldCheck["urlMatchesRegex"]["required"] = true;
			$fieldCheck["urlMatchesRegex"]["value"] = $pluginInstanceUrlMatchesRegex;
			if(self::url_matches_regex($onlinetext, $pluginInstanceUrlMatchesRegex))
			{
				$fieldCheck["urlMatchesRegex"]["check"] = true;
			}
			$overall = $overall && $fieldCheck["urlMatchesRegex"]["check"];
		}

		if($pluginInstanceMinChar)
		{
			$fieldCheck["minChar"]["required"] = true;
			$fieldCheck["minChar"]["value"] = $pluginInstanceMinChar;
			if(self::url_char_count($onlinetext, $pluginInstanceMinChar, 0))
			{
				$fieldCheck["minChar"]["check"] = true;
			}
			$overall = $overall && $fieldCheck["minChar"]["check"];
		}

		if($pluginInstanceMaxChar)
		{
			$fieldCheck["maxChar"]["required"] = true;
			$fieldCheck["maxChar"]["value"] = $pluginInstanceMaxChar;
			if(self::url_char_count($onlinetext, 0, $pluginInstanceMaxChar))
			{
				$fieldCheck["maxChar"]["check"] = true;
			}
			$overall = $overall && $fieldCheck["maxChar"]["check"];
		}

		if($pluginInstanceMinWords)
		{
			$fieldCheck["minWords"]["required"] = true;
			$fieldCheck["minWords"]["value"] = $pluginInstanceMinWords;
			if(self::url_word_count($onlinetext, $pluginInstanceMinWords, 0))
			{
				$fieldCheck["minWords"]["check"] = true;
			}
			$overall = $overall && $fieldCheck["minWords"]["check"];
		}

		if($pluginInstanceMaxWords)
		{
			$fieldCheck["maxWords"]["required"] = true;
			$fieldCheck["maxWords"]["value"] = $pluginInstanceMaxWords;
			if(self::url_word_count($onlinetext, 0, $pluginInstanceMaxWords))
			{
				$fieldCheck["maxWords"]["check"] = true;
			}
			$overall = $overall && $fieldCheck["maxWords"]["check"];
		}

		$fieldCheck["overall"] = $overall;
		$fieldCheck["grade"] = ($overall ? $maxGrade/*GRADE_VALID*/ : GRADE_ERROR);
		$fieldCheck["completion"] = ($overall ? COMPLETION_STATE_VALID : COMPLETION_STATE_ERROR);

		return $fieldCheck;

	}

}