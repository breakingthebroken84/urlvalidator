<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The main assignmentchecker configuration form
 *
 * It uses the standard core Moodle formslib. For more info about them, please
 * visit: http://docs.moodle.org/en/Development:lib/formslib.php
 *
 * @package    mod_assignmentchecker
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/course/moodleform_mod.php');

/**
 * Module instance settings form
 *
 * @package    mod_assignmentchecker
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_assignmentchecker_mod_form extends moodleform_mod {

    /**
     * Defines forms elements
     */
    public function definition() {
        global $CFG;

        $mform = $this->_form;

        // Adding the "general" fieldset, where all the common settings are showed.
        $mform->addElement('header', 'general', get_string('general', 'form'));

        // Adding the standard "name" field.
        $mform->addElement('text', 'name', get_string('assignmentcheckername', 'assignmentchecker'), array('size' => '64'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEANHTML);
        }
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        $mform->addHelpButton('name', 'assignmentcheckername', 'assignmentchecker');

        // Adding the standard "intro" and "introformat" fields.
        if ($CFG->branch >= 29) {
            $this->standard_intro_elements();
        } else {
            $this->add_intro_editor();
        }

        // Adding the rest of assignmentchecker settings, spreading all them into this fieldset
        // ... or adding more fieldsets ('header' elements) if needed for better logic.
        //$mform->addElement('static', 'label1', 'assignmentcheckersetting1', 'Your assignmentchecker fields go here. Replace me!');
        $mform->addElement('header', 'assignmentcheckerfieldset', get_string('criteriafieldset', 'assignmentchecker'));

        global $COURSE;
        global $DB;
        global $PAGE;

        $PAGE->requires->js("/mod/assignmentchecker/assets/assignment_checker.js");
        $PAGE->requires->css("/mod/assignmentchecker/assets/assignment_checker.css");

        $assignments = $DB->get_records(
            "assign",
            array(
                "course" => $COURSE->id
            )
        );
        $workshops = $DB->get_records(
            "workshop",
            array(
                "course" => $COURSE->id
            )
        );
        //$assignments = array_merge($assignments,$workshops);
        $ass_values = array();
        foreach ($assignments as $key => $value) {
            $ass_values[$value->id] = $value->name;
        }
        asort($ass_values);
        //array_unshift($ass_values,"-");
        $ass_values[0] = "-";

        $wor_values = array();
        foreach ($workshops as $key => $value) {
            $wor_values[$value->id] = $value->name;
        }
        asort($wor_values);
        //array_unshift($wor_values,"-");
        $wor_values[0] = "-";


        // Assignment select 
        $mform->addElement('select', 'assignmentid', get_string('assigninstance', 'assignmentchecker'), $ass_values);
        $mform->addElement('select', 'workshopid', get_string('workshopinstance', 'assignmentchecker'), $wor_values);

        //var_dump($ass_values);
        //var_dump($wor_values);
        //var_dump($this->current);
        echo "<script>
            document.addEventListener('DOMContentLoaded', function(event) {
                var assignmentid = document.getElementById('id_assignmentid');
                var workshopid = document.getElementById('id_workshopid');                
                if(".$this->current->workshopid.")
                {
                    assignmentid.value = 0;
                }
            });
        </script>";

        // Controls settings
        $mform->addElement('advcheckbox', 'urlisunique', get_string('urlisunique', 'assignmentchecker'), '', array('group' => 1), array(0, 1));
        $mform->addElement('advcheckbox', 'urlexists', get_string('urlexists', 'assignmentchecker'), '', array('group' => 1), array(0, 1));
        $mform->addElement('advcheckbox', 'urliscorrect', get_string('urliscorrect', 'assignmentchecker'), '', array('group' => 1), array(0, 1));
        $mform->setDefault('urliscorrect', true);

        // Correctness settings
        $mform->addElement('text', 'urlcontains', get_string('urlcontains', 'assignmentchecker'), array('size' => '64'));
        $mform->addElement('text', 'urlstartswith', get_string('urlstartswith', 'assignmentchecker'), array('size' => '64'));
        $mform->addElement('text', 'urlendswith', get_string('urlendswith', 'assignmentchecker'), array('size' => '64'));
        $mform->addElement('text', 'urlmatchesregex', get_string('urlmatchesregex', 'assignmentchecker'), array('size' => '64'));

        $charcount = array();
        $charcount[] =& $mform->createElement('text', 'minchar', get_string('textchar', 'assignmentchecker'), array('size' => '64'));
        $charcount[] =& $mform->createElement('text', 'maxchar', get_string('charinterval', 'assignmentchecker'), array('size' => '64'));
        $mform->addGroup($charcount, 'char_count', get_string('countchars', 'assignmentchecker'), array(' '), false);

        $wordcount = array();
        $wordcount[] =& $mform->createElement('text', 'minwords', get_string('textwords', 'assignmentchecker'), array('size' => '64'));
        $wordcount[] =& $mform->createElement('text', 'maxwords', get_string('wordinterval', 'assignmentchecker'), array('size' => '64'));
        $mform->addGroup($wordcount, 'word_count', get_string('countwords', 'assignmentchecker'), array(' '), false);

        $instanceId = $this->current->id;

        // Apply to all ungraded assignment
        /*
        $mform->addElement('button', 'button_applytoall', get_string('applytoall', 'assignmentchecker'));
        $mform->addElement('hidden', 'input_applytoall', 'yes');
        $mform->setDefault('input_applytoall', $CFG->wwwroot.'/mod/assignmentchecker/classes/past_evaluation.php?id='.$instanceId);
        */

        $reportLink = false;

        // fetch course module id for assignment
        if($this->current->assignmentid)
        {
            $moduleId = $DB->get_record(
                "course_modules",
                array(
                    "course" => $COURSE->id,
                    "module" => 1,// index for "assign", see mdl_modules table
                    "instance" => $this->current->assignmentid
                )
            )->id;
            $reportLink = $CFG->wwwroot.'/mod/assignmentchecker/_reportAssignment.php?course='.$COURSE->id.'&module='.$moduleId.'&assignment='.$this->current->assignmentid;
        }

        // fetch course module id for workshop
        if($this->current->workshopid)
        {
            $moduleId = $DB->get_record(
                "course_modules",
                array(
                    "course" => $COURSE->id,
                    "module" => 22,// index for "assign", see mdl_modules table
                    "instance" => $this->current->workshopid
                )
            )->id;
            $reportLink = $CFG->wwwroot.'/mod/assignmentchecker/_reportWorkshop.php?course='.$COURSE->id.'&module='.$moduleId.'&workshop='.$this->current->workshopid;
        }

        global $USER;
        if($USER->id == 7005)
        {
            if($reportLink)
            {
                $mform->addElement('html', '
                    <div id="fgroup_id_report" class="fitem fitem_fgroup ">
                        <div class="fitemtitle">
                            <div class="fgrouplabel">
                                <!--<label>PREGRESSO</label>-->
                            </div>
                        </div>
                        <div class="felement fgroup" data-fieldtype="group">
                            <div style="margin:1rem 0;"><a class="btn btn-secondary" href="'.$reportLink.'" target="_blank">VISUALIZZA REPORT VALUTAZIONI</a></div>  
                        </div>
                    </div>');
            }
        }        

        // Add standard grading elements.
        //$this->standard_grading_coursemodule_elements();

        // Add standard elements, common to all modules.
        $this->standard_coursemodule_elements();

        // Add standard buttons, common to all modules.
        $this->add_action_buttons();
    }
}
