<?php

	if(isset($_POST["evaluation"]))
	{
		if($_POST["type"] == "assignment")
		{
			pastEvaluationAssignments();
		}
		if($_POST["type"] == "workshop")
		{
			pastEvaluationWorkshop();
		}

		echo 'purging caches...<br>';
		purge_all_caches();
		echo 'done!<br>';
		die();
	}

	function pastEvaluationAssignments()
	{
		$courseId = $_POST["courseId"];
		$courseModuleId = $_POST["courseModuleId"];
		$assignmentId = $_POST["assignmentId"];
		$author = json_decode($_POST['author']);
		$timestamp = $_POST['timestamp'];
		$reportDate = date('jS F Y h:i:s A 	', time());

		$assignmentChecker = new mod_assignmentchecker_observer();

		$totalItems = 0;
		$totalItemsGradedPositive = 0;
		$totalItemsGradedNegative = 0;
		$totalItemsCompletedPositive = 0;
		$totalItemsCompletedNegative = 0;

		$logValidations = "";

		foreach ($_POST as $key => $value) {
			$key_Exp = explode('-',$key);
			if($key_Exp[0] == 'item')
			{
				$item = json_decode($value);
				$userId = $key_Exp[1];
				$firstname = $item->firstname;
				$lastname = $item->lastname;
				$email = $item->email;
				$grade = $item->grade;
				$completion = $item->completion;
				//$logContent .= "\n".$timestamp." id: ".$userId." - (".$firstname." ".$lastname." ".$email.")		grade: ".$grade." - completion: ".$completion;
				$logValidations .= "\n".date('jS F Y h:i:s A 	', $timestamp)." userid: ".$userId."			grade: ".$grade." 		completion: ".$completion."		".$email;
				$totalItems++;
				if($grade == 100) {
					$totalItemsGradedPositive++;
				}
				else
				{
					$totalItemsGradedNegative++;
				}
				if($completion == 2)
				{
					$totalItemsCompletedPositive++;
				}
				else
				{
					$totalItemsCompletedNegative++;
				}


				//if($userId == 14840)
				{
					$assignmentChecker->set_completed_course_module($userId, $courseModuleId, $item->completion, $timestamp);
					$assignmentChecker->set_assignment_grade($item->grade, $userId, $assignmentId, $timestamp);
					/*
					echo $userId;
					echo '<br>';
					echo $courseModuleId;
					echo '<br>';
					echo $assignmentId;
					echo '<br>';
					echo $item->grade;
					echo '<br>';
					echo $item->completion;
					echo '<br>';
					echo $timestamp;
					echo '<br>';
					*/
				}
			}
		}

		$logContent = "\n\n\n------------------------------------------------------------------------------------------------------------------------";
		$logContent .= "\nREPORT ".$reportDate. " author id: ". $author[0]." - ".$author[3];
		$logContent .= "\n  total:			".$totalItems;
		$logContent .= "\n  graded ok: 		".$totalItemsGradedPositive;
		$logContent .= "\n  graded ko: 		".$totalItemsGradedNegative;
		$logContent .= "\n  completion ok: 	".$totalItemsCompletedPositive;
		$logContent .= "\n  completion ko: 	".$totalItemsCompletedNegative;
		$logContent .= "\n------------------------------------------------------------------------------------------------------------------------";
		$logContent .= $logValidations;

		$file = dirname(__FILE__).'/log/log-course-'.$courseId.'-assignment-'.$_POST["assignmentId"].'.txt';
		if (file_exists($file)) {
		    $fh = fopen($file, 'a');
		    fwrite($fh, $logContent);
		} else {
		    $fh = fopen($file, 'wb');
		    fwrite($fh, $logContent);
		}


		echo 'valutazione pregressi terminata (Assignment)<br>';
	}

	function pastEvaluationWorkshop()
	{	
		$courseId = $_POST["courseId"];
		$courseModuleId = $_POST["courseModuleId"];
		$workshopId = $_POST["workshopId"];
		$author = json_decode($_POST['author']);
		$timestamp = $_POST['timestamp'];
		$reportDate = date('jS F Y h:i:s A 	', time());

		$assignmentChecker = new mod_assignmentchecker_observer();

		$totalItems = 0;
		$totalItemsGradedPositive = 0;
		$totalItemsGradedNegative = 0;
		$totalItemsCompletedPositive = 0;
		$totalItemsCompletedNegative = 0;

		$logValidations = "";

		foreach ($_POST as $key => $value) {
			$key_Exp = explode('-',$key);
			if($key_Exp[0] == 'item')
			{
				$item = json_decode($value);
				$userId = $key_Exp[1];
				$firstname = $item->firstname;
				$lastname = $item->lastname;
				$email = $item->email;
				$grade = $item->grade;
				$completion = $item->completion;
				//$logContent .= "\n".$timestamp." id: ".$userId." - (".$firstname." ".$lastname." ".$email.")		grade: ".$grade." - completion: ".$completion;
				$logValidations .= "\n".date('jS F Y h:i:s A 	', $timestamp)." userid: ".$userId."			grade: ".$grade." 		completion: ".$completion."		".$email;
				$totalItems++;
				if($grade) {
					$totalItemsGradedPositive++;
				}
				else
				{
					$totalItemsGradedNegative++;
				}
				if($completion == 2)
				{
					$totalItemsCompletedPositive++;
				}
				else
				{
					$totalItemsCompletedNegative++;
				}


				//if($userId == 364)
				{
					$assignmentChecker->set_completed_course_module($userId, $courseModuleId, $item->completion, $timestamp);
					$assignmentChecker->set_workshop_grade($item->grade, $userId, $workshopId, $author[0], $timestamp);

					//echo $item->grade.'<br>';
					//echo $userId.'<br>';
					//echo $workshopId.'<br>';
					//echo $author[0].'<br>';

					/*
					echo $userId;
					echo '<br>';
					echo $courseModuleId;
					echo '<br>';
					echo $assignmentId;
					echo '<br>';
					echo $item->grade;
					echo '<br>';
					echo $item->completion;
					echo '<br>';
					echo $timestamp;
					echo '<br>';
					*/
				}
			}
		}

		$logContent = "\n\n\n------------------------------------------------------------------------------------------------------------------------";
		$logContent .= "\nREPORT ".$reportDate. " author id: ". $author[0]." - ".$author[3];
		$logContent .= "\n  total:			".$totalItems;
		$logContent .= "\n  graded ok: 		".$totalItemsGradedPositive;
		$logContent .= "\n  graded ko: 		".$totalItemsGradedNegative;
		$logContent .= "\n  completion ok: 	".$totalItemsCompletedPositive;
		$logContent .= "\n  completion ko: 	".$totalItemsCompletedNegative;
		$logContent .= "\n------------------------------------------------------------------------------------------------------------------------";
		$logContent .= $logValidations;

		$file = dirname(__FILE__).'/log/log-course-'.$courseId.'-workshop-'.$_POST["workshopId"].'.txt';
		if (file_exists($file)) {
		    $fh = fopen($file, 'a');
		    fwrite($fh, $logContent);
		} else {
		    $fh = fopen($file, 'wb');
		    fwrite($fh, $logContent);
		}


		echo 'valutazione pregressi terminata (Workshop)<br>';
	}
