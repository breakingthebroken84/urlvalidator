<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * English strings for assignmentchecker
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_assignmentchecker
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['modulename'] = 'Assignment Checker';
$string['modulenameplural'] = 'Assignment Checkers';
$string['modulename_help'] = 'Use the Assignment Checker module for... | The Assignment Checker module allows...';
$string['assignmentchecker:addinstance'] = 'Add a new Assignment Checker';
$string['assignmentchecker:submit'] = 'Submit Assignment Checker';
$string['assignmentchecker:view'] = 'View Assignment Checker';
$string['assignmentcheckerfieldset'] = 'Custom example fieldset';
$string['assignmentcheckername'] = 'Assignment Checker name';
$string['assignmentcheckername_help'] = 'This is the content of the help tooltip associated with the assignmentcheckername field. Markdown syntax is supported.';
$string['assignmentchecker'] = 'Assignment Checker';
$string['pluginadministration'] = 'Assignment Checker administration';
$string['pluginname'] = 'Assignment Checker';

$string['criteriafieldset'] = 'Impostazioni di Validazione';
$string['urlisunique'] = 'Unicità';
$string['urlexists'] = 'Esistenza';
$string['urliscorrect'] = 'Correttezza';
$string['urlcontains'] = 'Contiene';
$string['urlstartswith'] = 'Inizia con';
$string['urlendswith'] = 'Finisce con';
$string['urlmatchesregex'] = 'Espressione regolare';
$string['assigninstance'] = 'Compito da valutare';
$string['workshopinstance'] = 'Workshop da valutare';
$string['applytoall'] = 'Applica a tutti';
$string['countchars'] = 'Conteggio caratteri';
$string['countwords'] = 'Conteggio parole';
$string['wordinterval'] = '<b>&le;</b>n. parole<b>&lt;</b>';
$string['charinterval'] = '<b>&le;</b>n. caratteri<b>&lt;</b>';